import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Principale {
	public static void main(String[] args){
		Modele m = new Modele();
		VueGraphique vg = new VueGraphique();
		VueBouton vb = new VueBouton();
		m.addObserver(vg);
		m.addObserver(vb);
		vg.setPreferredSize(new Dimension(300,300));
		
		JFrame f = new JFrame("Projet IHM");
		f.setLayout(new BorderLayout());
		f.add(vb,BorderLayout.NORTH);
		f.add(vg,BorderLayout.CENTER);
		f.pack();
		f.setSize(500, 500);
		f.setVisible(true);
	}
}
