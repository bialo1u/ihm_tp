import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class VueGraphique extends JPanel implements Observer{

	private Modele tabfigures;
	
	public VueGraphique(){
		super();
		this.tabfigures = new Modele();
	}
	
	public void paintComponent(Graphics arg0){
		super.paintComponent(arg0);
	}
	
	public void update(Observable arg0, Object arg1) {
		//tabfigures = (FigureColoree) arg0;
		repaint();
	}

}
