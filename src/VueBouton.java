import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class VueBouton extends JPanel implements Observer{
	
	private JRadioButton bt1,bt2,bt3;
	private JComboBox cb1,cb2;
	private JPanel p1, p2;
	
	public VueBouton() {
		super();
		p1 = new JPanel(new FlowLayout());
		p2 = new JPanel(new FlowLayout());
		bt1 = new JRadioButton("Nouvelle figure");
		bt2 = new JRadioButton("Tracé à main levée");
		bt3 = new JRadioButton("Manipulations");
		p1.add(bt1);
		p1.add(bt2);
		p1.add(bt3);
		cb1 = new JComboBox();
		cb2 = new JComboBox();
		p2.add(cb1);
		p2.add(cb2);
		setLayout(new BorderLayout());
		this.add(p1,BorderLayout.NORTH);
		this.add(p2,BorderLayout.CENTER);
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	
}
