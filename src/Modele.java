import java.awt.*;
import java.util.*;

public class Modele {
	private ArrayList<FigureColoree> fc;
	
	public Modele() {
		this.fc = new ArrayList<FigureColoree>();
	}
	
	public void ajouterFigure(FigureColoree pfc) {
		this.fc.add(pfc);
	}
	
	public void supprimerFigure(FigureColoree pfc) {
		this.fc.remove(pfc);
	}
	
	public void changerCouleur(FigureColoree pfc, Color pc) {
		this.fc.get(this.fc.indexOf(pfc)).changeCouleur(pc);
	}
	
	public ArrayList<FigureColoree> getModele() {
		return this.fc;
	}
	
	public void setModele(ArrayList<FigureColoree> pfc) {
		this.fc = pfc;
	}
}
