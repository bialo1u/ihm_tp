
public class Point {
	private int x,y;
	
	public Point(int px,int py){
		x = px;
		y = py;
	}
	
	public double distance(Point p){
		double res = (p.rendreX()-this.x)*(p.rendreX()-this.x)+(p.rendreY()-this.y)*(p.rendreY()-this.y);
		res = Math.sqrt(res);
		return res;
	}
	
	public int rendreX(){
		return x;
	}
	
	public int rendreY(){
		return y;
	}
	
	public void incrementerX(int px){
		x += px;
	}
	
	public void incrementerY(int py){
		y += py;
	}
	
	public void modifierX(int px){
		x = px;
	}
	
	public void modifierY(int py){
		y = py;
	}
	
	public void translation(int px, int py){
		x += px;
		y += py;
	}
}
