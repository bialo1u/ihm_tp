import java.awt.*;

public abstract class FigureColoree {
	private final static int TAILLE_CARRE_SELECTION = 0;
	private final static int PERIPHERIE_CARRE_SELECTION = 0;
	private boolean selection;
	protected Color couleur;
	
	public FigureColoree() {
		throw new Error("A completer");
	}
	
	public abstract int nbPoint();
	
	public abstract int nbClics();
	
	public abstract boolean estDedans(int i, int j);
	
	public abstract void modifierPoint(Point[] p);
	
	public void affiche(Graphics g) {
		throw new Error("A completer");
	}
	
	public void translation(int i, int j) {
		throw new Error("A completer");
	}
	
	public void transformation(int i, int j, int k) {
		throw new Error("A completer");
	}
	
	public int carreDeSelection(int i, int j) {
		throw new Error("A completer");
	}
	
	public void selectionne() {
		
	}
	
	public void deSelectionne() {
		throw new Error("A completer");
	}
	
	public void changeCouleur(Color c) {
		throw new Error("A completer");
	}
}
